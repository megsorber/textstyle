package com.javafxcontrols.textstyle;
/* 
 * RabbitMoto LLC
 * 2017 - Present
 * All Right Reserved.
 * 
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.pdf', which is included with this package.
 * 
*/

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import com.javafxcontrols.textstyle.TextStyle;
import com.javafxcontrols.textstyle.TextStyleController;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class TextStyleExample extends Application {
	TextStyleController TextStyleController = new TextStyleController();
	TextArea textArea = new TextArea("TextArea1\nTextArea2");
	TextField textField = new TextField("TextField");
	Text text = new Text("Text1\nText2");
	TextFlow textflow = new TextFlow();

	@Override
	public void start(Stage stage) throws Exception {
		// NOTE Bug: https://bugs.openjdk.java.net/browse/JDK-8091064
		// Fonts that don't have a matching italic/bold font will be
		// rendered with the base font. If this is an issue use the css
		// style which does implement synthetic font styling

		TextStyle textStyle = TextStyleController.textStyleProperty().get();

		double width = 1000;
		VBox vbox = new VBox();
		vbox.getChildren().add(TextStyleController);

		TextStyleController.textStyleProperty.get().addPropertyChangeListener(new PropertyChangeListenerExample());

		textField.minWidth(width - 100);
		vbox.getChildren().add(textField);

		// NOTE1: The text will be aligned according to the widest line
		// so changing the textAlignment will have no effect on a text object
		// that is only 1 line.
		// NOTE2: text objects do not support a background color
		// To work around these issues the TextStyle class has an
		// alignment property that can be used to align a pane containing
		// the text and to change that pane's background color.
		HBox hbox = new HBox();
		hbox.alignmentProperty().bind(textStyle.alignmentProperty());
		hbox.backgroundProperty().bind(textStyle.highlightProperty());
		hbox.getChildren().add(text);
		vbox.getChildren().add(hbox);

		textflow.minWidth(width - 100);
		textflow.getChildren().add(new Text("TextFlow1"));
		textflow.getChildren().add(new Text("\n"));
		textflow.getChildren().add(new Text("TextFlow2"));
		vbox.getChildren().add(textflow);

		textArea.maxHeight(10);
		vbox.getChildren().add(textArea);

		Scene scene = new Scene(vbox);
		stage.setScene(scene);
		stage.setTitle("TextStyle Example");
		stage.setWidth(width);
		stage.setHeight(600);
		stage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

	class PropertyChangeListenerExample implements PropertyChangeListener {

		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			TextStyleController.getTextStyle().applyTextStyle(text);
			TextStyleController.getTextStyle().applyTextStyle(textArea);
			TextStyleController.getTextStyle().applyTextStyle(textField);
			TextStyleController.getTextStyle().applyTextStyle(textflow);
		}
	}
}
