package com.javafxcontrols.textstyle;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.util.Callback;
import javafx.util.StringConverter;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class TextStyleController extends HBox {
	private ObjectProperty<EventHandler<ActionEvent>> onActionProperty = new SimpleObjectProperty<EventHandler<ActionEvent>>();

	public final ObjectProperty<TextStyle> textStyleProperty = new SimpleObjectProperty<TextStyle>(new TextStyle());
	@FXML
	protected ToggleButton boldTB;
	@FXML
	protected ToggleButton italicTB;
	@FXML
	protected ToggleButton underlineTB;
	@FXML
	protected ComboBox<Double> fontSizeCB;
	@FXML
	protected ComboBox<String> fontsCB;
	@FXML
	protected Button fillBtn;
	@FXML
	protected ColorPicker fillCP;
	@FXML
	protected Button highlightBtn;
	@FXML
	protected ColorPicker highlightCP;
	@FXML
	protected ComboBox<Double> lineSpacingCB;
	@FXML
	protected ComboBox<TextAlignment> textAlignmentCB;

	private Map<TextAlignment, Image> alignments = new HashMap<TextAlignment, Image>();
	CornerRadii defaultCorners;
	Insets defaultInsets;

	public TextStyleController() {
		super();

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("TextStyle.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();

			// the css classes need to be added to the scene but the
			// scene is not yet set here. Load the relevant css files
			// on the first use of the option
			underlineTB.addEventHandler(ActionEvent.ACTION, this::initializeUnderlineCSS);
			textAlignmentCB.addEventHandler(ActionEvent.ACTION, this::initializeAlignmentCSS);
			highlightCP.addEventHandler(ActionEvent.ACTION, this::initializeHighlightCSS);
			fillCP.addEventHandler(ActionEvent.ACTION, this::initializeFill);

			textStyleProperty.get().boldProperty().bind(boldTB.selectedProperty());
			textStyleProperty.get().underlineProperty().bind(underlineTB.selectedProperty());
			textStyleProperty.get().italicProperty().bind(italicTB.selectedProperty());
			textStyleProperty.get().fontSizeProperty().bind(fontSizeCB.valueProperty());
			textStyleProperty.get().fontFamilyProperty().bind(fontsCB.valueProperty());
			textStyleProperty.get().fillProperty().bind(fillCP.valueProperty());
			textStyleProperty.get().lineSpacingProperty().bind(lineSpacingCB.valueProperty());
			textStyleProperty.get().textAlignmentProperty().bind(textAlignmentCB.valueProperty());

			fillCP.setValue(Color.BLACK);
			fillCP.setPromptText("Font Color");

			fontsCB.getSelectionModel().select(Font.getDefault().getFamily());
			fontsCB.getSelectionModel().select(Font.getDefault().getName());
			fontsCB.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
				@Override
				public ListCell<String> call(ListView<String> list) {
					return new FontFormatCell();
				}
			});
			fontsCB.getItems().addAll(Font.getFamilies());

			fontSizeCB.setEditable(true);
			fontSizeCB.setConverter(new SizeConverter());
			fontSizeCB.getSelectionModel().select(4);

			lineSpacingCB.setEditable(true);
			lineSpacingCB.setConverter(new SizeConverter());
			lineSpacingCB.getSelectionModel().select(0);

			setupAlignments();

			textStyleProperty.get().updateFontStyle();

			highlightCP.getStyleClass().add(ColorPicker.STYLE_CLASS_BUTTON);
			highlightBtn.toFront();

			fillCP.getStyleClass().add(ColorPicker.STYLE_CLASS_BUTTON);
			fillBtn.toFront();

		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
	}

	private void setupAlignments() {
		Image leftAlignIcon = new Image(getClass().getResourceAsStream("icons/leftAlign.png"));
		alignments.put(TextAlignment.LEFT, leftAlignIcon);

		Image centerAlignIcon = new Image(getClass().getResourceAsStream("icons/centerAlign.png"));
		alignments.put(TextAlignment.CENTER, centerAlignIcon);

		Image rightAlignIcon = new Image(getClass().getResourceAsStream("icons/rightAlign.png"));
		alignments.put(TextAlignment.RIGHT, rightAlignIcon);

		Image justifyIcon = new Image(getClass().getResourceAsStream("icons/justify.png"));
		alignments.put(TextAlignment.JUSTIFY, justifyIcon);

		textAlignmentCB.getItems().addAll(TextAlignment.LEFT, TextAlignment.CENTER, TextAlignment.RIGHT,
				TextAlignment.JUSTIFY);
		textAlignmentCB.setCellFactory(new Callback<ListView<TextAlignment>, ListCell<TextAlignment>>() {
			@Override
			public ListCell<TextAlignment> call(ListView<TextAlignment> list) {
				return new AlignFormatCell();
			}
		});
		textAlignmentCB.getSelectionModel().select(0);
		textAlignmentCB.setButtonCell(new AlignFormatCell());
	}

	class AlignFormatCell extends ListCell<TextAlignment> {

		private final VBox cell;
		private final ImageView imageview;
		private Tooltip tooltip;

		public AlignFormatCell() {
			setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
			cell = new VBox();
			cell.setPadding(Insets.EMPTY);
			imageview = new ImageView();
			imageview.setFitHeight(16.5);
			imageview.setFitWidth(16.5);
			imageview.setVisible(true);
			imageview.setCache(true);
			cell.getChildren().addAll(imageview);
			tooltip = new Tooltip("");
			Tooltip.install(cell, tooltip);
			setGraphic(cell);
		}

		@Override
		protected void updateItem(TextAlignment item, boolean empty) {
			super.updateItem(item, empty);

			if (item == null || empty) {
				setText(null);
				setGraphic(null);
			} else {
				imageview.setImage(alignments.get(item));
				tooltip.setText(item.toString());
				setGraphic(cell);
			}
		}
	}

	public ObjectProperty<EventHandler<ActionEvent>> onActionProperty() {
		return onActionProperty;
	}

	public void setOnAction(EventHandler<ActionEvent> handler) {
		onActionProperty.set(handler);
	}

	public EventHandler<ActionEvent> getOnAction() {
		return onActionProperty.get();
	}

	class FontFormatCell extends ListCell<String> {

		public FontFormatCell() {
		}

		@Override
		protected void updateItem(String fontFamily, boolean empty) {
			super.updateItem(fontFamily, empty);
			if (fontFamily != null) {
				setText(fontFamily);
				FontWeight fontWeight = (boldTB.isSelected()) ? FontWeight.BOLD : FontWeight.NORMAL;
				FontPosture fontPosture = (italicTB.isSelected()) ? FontPosture.ITALIC : FontPosture.REGULAR;
				this.setFont(Font.font(fontFamily, fontWeight, fontPosture, 12));
			}
		}
	}

	class SizeConverter extends StringConverter<Double> {

		@Override
		public String toString(Double number) {
			return number.toString();
		}

		@Override
		public Double fromString(String string) {
			return Double.parseDouble(string);
		}
	}

	public TextStyle getTextStyle() {
		return textStyleProperty.get();
	}

	public void setTextStyle(TextStyle newTextStyle) {
		textStyleProperty.set(newTextStyle);
	}

	public ObjectProperty<TextStyle> textStyleProperty() {
		return textStyleProperty;
	}

	@FXML
	protected void openFillCP(ActionEvent event) {
		fillCP.show();
	}

	@FXML
	protected void updateFill(ActionEvent event) {
		fillBtn.setTextFill(fillCP.getValue());
		Color fillColor = isDark(fillCP.getValue()) ? Color.rgb(227, 227, 227) : Color.DARKGRAY;
		BackgroundFill newFill = new BackgroundFill(fillColor, defaultCorners, defaultInsets);
		Background newFillBackground = new Background(newFill);
		fillBtn.setBackground(newFillBackground);
	}

	@FXML
	protected void openHighlightCP(ActionEvent event) {
		highlightCP.show();
	}

	@FXML
	protected void updateHighlight(ActionEvent event) {
		TextStyle textStyle = textStyleProperty().get();
		BackgroundFill newHighlightFill = new BackgroundFill(highlightCP.getValue(), defaultCorners, defaultInsets);
		Background newHighlight = new Background(newHighlightFill);
		textStyle.setHighlight(newHighlight);
		highlightBtn.setBackground(newHighlight);

		if (isDark(highlightCP.getValue())) {
			highlightBtn.setTextFill(Color.WHITE);
		} else {
			highlightBtn.setTextFill(Color.BLACK);
		}
	}

	private void initializeFill(ActionEvent event) {
		if (defaultCorners == null) {
			defaultCorners = highlightBtn.getBackground().getFills().get(0).getRadii();
			defaultInsets = highlightBtn.getBackground().getFills().get(0).getInsets();
		}

		this.removeEventHandler(ActionEvent.ACTION, this::initializeFill);
	}

	public void initializeAlignmentCSS(ActionEvent event) {
		boldTB.getScene().getStylesheets().add(getClass().getResource("css/alignment.css").toExternalForm());
		this.removeEventHandler(ActionEvent.ACTION, this::initializeAlignmentCSS);
	}

	public void initializeUnderlineCSS(ActionEvent event) {
		textAlignmentCB.getScene().getStylesheets().add(getClass().getResource("css/underline.css").toExternalForm());
		this.removeEventHandler(ActionEvent.ACTION, this::initializeUnderlineCSS);
	}

	public void initializeHighlightCSS(ActionEvent event) {
		if (defaultCorners == null) {
			defaultCorners = highlightBtn.getBackground().getFills().get(0).getRadii();
			defaultInsets = highlightBtn.getBackground().getFills().get(0).getInsets();
		}

		highlightCP.getScene().getStylesheets()
				.add(getClass().getResource("css/textAreaBackground.css").toExternalForm());
		this.removeEventHandler(ActionEvent.ACTION, this::initializeHighlightCSS);
	}

	private boolean isDark(Color c) {
		double brightness = Math.sqrt(
				c.getRed() * c.getRed() * .241 + c.getGreen() * c.getGreen() * .691 + c.getBlue() * c.getBlue() * .068);
		return brightness <= .5;
	}
}
