package com.javafxcontrols.textstyle;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextBoundsType;
import javafx.scene.text.TextFlow;

public class TextStyle implements PropertyChangeListener {

	private BooleanProperty bold = new SimpleBooleanProperty(false);
	private BooleanProperty underline = new SimpleBooleanProperty(false);
	private BooleanProperty italic = new SimpleBooleanProperty(false);
	private DoubleProperty fontSize = new SimpleDoubleProperty(12);
	private StringProperty fontFamily = new SimpleStringProperty(Font.getDefault().getFamily());
	private ObjectProperty<Color> fill = new SimpleObjectProperty<Color>(Color.BLACK);
	private ObjectProperty<Background> highlight = new SimpleObjectProperty<Background>(
			new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
	private StringProperty fontStyle = new SimpleStringProperty();
	private ObjectProperty<Font> font = new SimpleObjectProperty<Font>(Font.getDefault());
	private DoubleProperty lineSpacing = new SimpleDoubleProperty(1);
	private ObjectProperty<TextAlignment> textAlignment = new SimpleObjectProperty<TextAlignment>(TextAlignment.LEFT);
	private ObjectProperty<Pos> alignment = new SimpleObjectProperty<Pos>(Pos.CENTER_LEFT);

	private PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.removePropertyChangeListener(listener);
	}

	@Override
	public void propertyChange(PropertyChangeEvent arg0) {
		System.out.println("PropertyChangeEvent");
	}

	public TextStyle() {
		// The set<property> methods aren't getting called when they
		// are updated via binding. So instead of updating the style
		// and firing a change event there do so with change listeners.
		boldProperty().addListener(new BoldChangeListener());
		underlineProperty().addListener(new UnderlineChangeListener());
		italicProperty().addListener(new ItalicChangeListener());
		fontSizeProperty().addListener(new FontSizeChangeListener());
		fontFamilyProperty().addListener(new FontFamilyChangeListener());
		fillProperty().addListener(new FillChangeListener());
		highlightProperty().addListener(new HighlightChangeListener());
		lineSpacingProperty().addListener(new LineSpacingChangeListener());
		textAlignmentProperty().addListener(new TextAlignmentChangeListener());
	}

	class BoldChangeListener implements ChangeListener<Object> {
		@Override
		public void changed(ObservableValue<?> arg0, Object oldVal, Object newVal) {
			updateFontStyle();
			pcs.firePropertyChange("bold", oldVal, newVal);

		}
	}

	class UnderlineChangeListener implements ChangeListener<Object> {
		@Override
		public void changed(ObservableValue<?> arg0, Object oldVal, Object newVal) {
			updateFontStyle();
			pcs.firePropertyChange("underline", oldVal, newVal);
		}
	}

	class ItalicChangeListener implements ChangeListener<Object> {
		@Override
		public void changed(ObservableValue<?> arg0, Object oldVal, Object newVal) {
			updateFontStyle();
			pcs.firePropertyChange("italic", oldVal, newVal);
		}
	}

	class FontSizeChangeListener implements ChangeListener<Object> {
		@Override
		public void changed(ObservableValue<?> arg0, Object oldVal, Object newVal) {
			updateFontStyle();
			pcs.firePropertyChange("fontSize", oldVal, newVal);
		}
	}

	class FontFamilyChangeListener implements ChangeListener<Object> {
		@Override
		public void changed(ObservableValue<?> arg0, Object oldVal, Object newVal) {
			updateFontStyle();
			pcs.firePropertyChange("fontFamily", oldVal, newVal);
		}
	}

	class FillChangeListener implements ChangeListener<Object> {
		@Override
		public void changed(ObservableValue<?> arg0, Object oldVal, Object newVal) {
			updateFontStyle();
			pcs.firePropertyChange("fill", oldVal, newVal);
		}
	}

	class HighlightChangeListener implements ChangeListener<Object> {
		@Override
		public void changed(ObservableValue<?> arg0, Object oldVal, Object newVal) {
			updateFontStyle();
			pcs.firePropertyChange("highlight", oldVal, newVal);
		}
	}

	class LineSpacingChangeListener implements ChangeListener<Object> {
		@Override
		public void changed(ObservableValue<?> arg0, Object oldVal, Object newVal) {
			updateFontStyle();
			pcs.firePropertyChange("lineSpacing", oldVal, newVal);
		}
	}

	class TextAlignmentChangeListener implements ChangeListener<Object> {
		@Override
		public void changed(ObservableValue<?> arg0, Object oldVal, Object newVal) {
			updateFontStyle();
			pcs.firePropertyChange("textAlignment", oldVal, newVal);

			Pos oldAlignment = alignment.get();
			if (textAlignment.getValue() == TextAlignment.LEFT || textAlignment.getValue() == TextAlignment.JUSTIFY) // No
																														// support
																														// for
																														// justify
				alignment.set(Pos.CENTER_LEFT);
			else if (textAlignment.getValue() == TextAlignment.CENTER)
				alignment.set(Pos.CENTER);
			else if (textAlignment.getValue() == TextAlignment.RIGHT)
				alignment.set(Pos.CENTER_RIGHT);
			pcs.firePropertyChange("alignment", oldAlignment, alignment);
		}
	}

	public Boolean getBold() {
		return bold.get();
	}

	public void setBold(Boolean newBold) {
		System.out.println("test");
		bold.set(newBold);
	}

	public BooleanProperty boldProperty() {
		return bold;
	}

	public Boolean getUnderline() {
		return underline.get();
	}

	public void setUnderline(Boolean newUnderline) {
		underline.set(newUnderline);
	}

	public BooleanProperty underlineProperty() {
		return underline;
	}

	public Boolean getItalic() {
		return italic.get();
	}

	public void setItalic(Boolean newItalic) {
		italic.set(newItalic);
	}

	public BooleanProperty italicProperty() {
		return italic;
	}

	public Double getFontSize() {
		return fontSize.get();
	}

	public void setFontSize(Double newFontSize) {
		fontSize.set(newFontSize);
	}

	public DoubleProperty fontSizeProperty() {
		return fontSize;
	}

	public String getFontFamily() {
		return fontFamily.get();
	}

	public void setFontFamily(String newFontFamily) {
		fontFamily.set(newFontFamily);
	}

	public StringProperty fontFamilyProperty() {
		return fontFamily;
	}

	public Color getFill() {
		return fill.get();
	}

	public void setFill(Color newColor) {
		fill.set(newColor);
	}

	public ObjectProperty<Color> fillProperty() {
		return fill;
	}

	public Background getHighlight() {
		return highlight.get();
	}

	public void setHighlight(Background newColor) {
		highlight.set(newColor);
	}

	public ObjectProperty<Background> highlightProperty() {
		return highlight;
	}

	public Font getFontProperty() {
		return font.getValue();
	}

	public void setFont(Font font) {
		this.font.setValue(font);
	}

	public ObjectProperty<Font> fontProperty() {
		return font;
	}

	public String getFontStyleProperty() {
		return fontStyle.getValue();
	}

	public void setFontStyle(String style) {
		fontStyle.setValue(style);
	}

	public StringProperty fontStyleProperty() {
		return fontStyle;
	}

	public Double getLineSpacing() {
		return lineSpacing.get();
	}

	public void setLineSpacing(Double newLineSpacing) {
		lineSpacing.set(newLineSpacing);
	}

	public DoubleProperty lineSpacingProperty() {
		return lineSpacing;
	}

	public TextAlignment getTextAlignment() {
		return textAlignment.get();
	}

	public void setTextAlignment(TextAlignment newTextAlignment) {
		textAlignment.set(newTextAlignment);
	}

	public ObjectProperty<TextAlignment> textAlignmentProperty() {
		return textAlignment;
	}

	public Pos getAlignment() {
		return alignment.get();
	}

	public ObjectProperty<Pos> alignmentProperty() {
		return alignment;
	}

	protected void updateFontStyle() {
		FontWeight fontWeight = (bold.getValue()) ? FontWeight.BOLD : FontWeight.NORMAL;
		FontPosture fontPosture = (italic.getValue()) ? FontPosture.ITALIC : FontPosture.REGULAR;
		String fontPostureString = (italic.getValue()) ? "italic" : "normal";
		Font font = Font.font(fontFamily.getValue(), fontWeight, fontPosture, fontSize.getValue());
		this.font.setValue(font);

		double lineHeight = getTextHeight("A") / 2;

		fontStyle.setValue("-fx-font-weight: " + fontWeight + ";\n" + "-fx-font-style: " + fontPostureString + ";\n"
				+ "-fx-underline: " + underline.getValue() + ";\n" + "-fx-font-size: " + fontSize.getValue() + ";\n"
				+ "-fx-font-family: " + fontFamily.getValue() + ";\n" + "-fx-fill: " + toRGB(fill.getValue()) + ";\n"
				+ "-fx-text-inner-color: " + toRGB(fill.getValue()) + ";\n" + "-fx-line-spacing: "
				+ (lineSpacing.getValue() - 1) * lineHeight + ";\n" + "-fx-text-alignment: " + textAlignment.getValue()
				+ ";\n" + "-fx-background-color: " + toRGB((Color) highlight.getValue().getFills().get(0).getFill())
				+ ";\n");
	}

	private String toRGB(Color color) {
		return String.format("#%02X%02X%02X", (int) (color.getRed() * 255), (int) (color.getGreen() * 255),
				(int) (color.getBlue() * 255));
	}

	// get the width of the text with the current style
	public double getTextWidth(String text) {
		return TextUtils.computeTextWidth(font.getValue(), text, 0.0D);
	}

	// get the height of the text with the current style
	public double getTextHeight(String text) {
		return TextUtils.computeTextHeight(font.getValue(), text, 0.0D);
	}

	String cssBackgroundColor = "-fx-background-color: ";
	String cssfill = "-fx-text-inner-color: ";
	String cssTextAreaBackground = "text-area-background: ";

	private String removeCSSProperty(String originalCSS, String cssProperty) {
		String newStyle = originalCSS;
		int propertyIndex = originalCSS.indexOf(cssProperty);

		if (propertyIndex != -1) {
			newStyle = originalCSS.substring(0, propertyIndex);
			int indexOfColon = originalCSS.substring(propertyIndex).indexOf(";");
			if (indexOfColon + 1 < originalCSS.length())
				newStyle += originalCSS.substring(indexOfColon + 1);
		}
		return newStyle;
	}

	public void removeTextStyleCSS(String originalCSS, String orginalCssClasses) {

	}

	public void applyTextStyle(Text text) {
		text.fontProperty().set(font.getValue()); // covers bold/italic/font size/font family
		text.underlineProperty().set(underline.getValue());
		text.fillProperty().set(fill.getValue());
		text.lineSpacingProperty().set((lineSpacing.getValue() - 1) * getTextHeight("A") / 2);
		text.textAlignmentProperty().set(textAlignment.getValue());
	}

	public void applyTextStyle(TextField textField) {
		textField.fontProperty().set(font.getValue()); // covers bold/italic/font size/font family

		String cssStyle = removeCSSProperty(textField.getStyle(), cssfill);
		cssStyle += cssfill + toRGB(fill.getValue()) + ";";
		textField.setStyle(cssStyle);

		textField.getStyleClass().remove("underline");
		if (underline.get()) {
			textField.getStyleClass().add("underline");
		}

		textField.setAlignment(alignment.getValue());
		textField.setBackground(highlight.get());
	}

	public void applyTextStyle(TextFlow textFlow) {
		for (Node node : textFlow.getChildren()) {
			Text text = (Text) node;
			text.fontProperty().set(font.getValue()); // covers bold/italic/font size/font family
			text.underlineProperty().set(underline.getValue());
			text.fillProperty().set(fill.getValue());
		}

		textFlow.lineSpacingProperty().set((lineSpacing.getValue() - 1) * getTextHeight("A") / 2);
		textFlow.textAlignmentProperty().set(textAlignment.getValue());
		textFlow.setBackground(highlight.get());
	}

	public void applyTextStyle(TextArea textArea) {
		textArea.fontProperty().set(font.getValue()); // covers bold/italic/font size/font family

		textArea.getStyleClass().remove("underline");
		if (underline.get()) {
			textArea.getStyleClass().add("underline");
		}

		textArea.getStyleClass().removeAll("LEFT", "CENTER", "RIGHT", "JUSTIFY");
		textArea.getStyleClass().add(textAlignment.getValue().toString());

		String cssStyle = removeCSSProperty(textArea.getStyle(), cssfill);
		cssStyle += cssfill + toRGB(fill.getValue()) + ";";
		textArea.setStyle(cssStyle);

		cssStyle = removeCSSProperty(textArea.getStyle(), cssTextAreaBackground);
		cssStyle += cssTextAreaBackground + toRGB((Color) highlight.getValue().getFills().get(0).getFill()) + ";";
		textArea.setStyle(cssStyle);

		// TODO: Is there a way around the TextArea's lack of support for line spacing?
	}
}

class TextUtils {

	static final Text helper;
	static final double DEFAULT_WRAPPING_WIDTH;
	static final double DEFAULT_LINE_SPACING;
	static final String DEFAULT_TEXT;
	static final TextBoundsType DEFAULT_BOUNDS_TYPE;
	static final double DEFAULT_SIZE;
	static {
		helper = new Text();
		DEFAULT_WRAPPING_WIDTH = helper.getWrappingWidth();
		DEFAULT_LINE_SPACING = helper.getLineSpacing();
		DEFAULT_TEXT = helper.getText();
		DEFAULT_BOUNDS_TYPE = helper.getBoundsType();
		DEFAULT_SIZE = helper.getFont().getSize();
	}

	public static double computeTextWidth(Font font, String text, double help0) {

		helper.setText(text);
		helper.setFont(font);

		helper.setWrappingWidth(0.0D);
		helper.setLineSpacing(0.0D);
		double d = Math.min(helper.prefWidth(-1.0D), help0);
		helper.setWrappingWidth((int) Math.ceil(d));
		d = Math.ceil(helper.getLayoutBounds().getWidth());

		helper.setWrappingWidth(DEFAULT_WRAPPING_WIDTH);
		helper.setLineSpacing(DEFAULT_LINE_SPACING);
		helper.setText(DEFAULT_TEXT);
		return d;
	}

	public static double computeTextHeight(Font font, String text, double help0) {

		helper.setText(text);
		helper.setFont(font);
		helper.setLineSpacing(0.0D);
		double d = Math.ceil(helper.getLayoutBounds().getHeight());

		helper.setLineSpacing(DEFAULT_LINE_SPACING);
		helper.setText(DEFAULT_TEXT);
		return d;
	}
}
